#!/usr/bin/env bash

script_path="$(dirname "$(realpath "$0")")"
. "$script_path/functions.sh"
assert_prerequisites

image_path="$1"
image_name="$(relative_path_to_image_name "$image_path")"

shift

# Load config if we have it
[ -f "$script_path/$image_path/control" ] && . "$script_path/$image_path/control"

extra_args=""

if [ ! -z "${INVALIDATE_CACHE_UPSTREAM_ARCHLINUX+x}" ]; then
  log "Fetching upstream package version from Arch Linux"
  upstream_ver="$(get_archlinux_pkg_ver "$INVALIDATE_CACHE_UPSTREAM_ARCHLINUX")"
elif [ ! -z "${INVALIDATE_CACHE_UPSTREAM_AUR+x}" ]; then
  log "Fetching upstream package version from AUR"
  upstream_ver="$(get_aur_pkg_ver "$INVALIDATE_CACHE_UPSTREAM_AUR")"
elif [ ! -z "${INVALIDATE_CACHE_UPSTREAM_UBUNTU+x}" ]; then
  log "Fetching upstream package version from Ubuntu (Launchpad)"
  upstream_ver="$(get_ubuntu_pkg_ver "$INVALIDATE_CACHE_UPSTREAM_UBUNTU")"
elif [ ! -z "${INVALIDATE_CACHE_UPSTREAM_GITHUB+x}" ]; then
  log "Fetching upstream package version from GitHub"
  upstream_ver="$(get_github_pkg_ver "$INVALIDATE_CACHE_UPSTREAM_GITHUB")"
fi

if [ ! -z "$upstream_ver" ]; then
  log "Latest upstream version: $upstream_ver"
  extra_args="$extra_args --build-arg UPSTREAM_VERSION=$upstream_ver"
fi

podman build -t "$image_name" "$script_path/$image_path" $extra_args "$@"
