#!/usr/bin/env bash

# wl-mitm (config is external but expected to listen on wayland-10)
echo "Starting wl-mitm"
wl-mitm /tmp/wl-mitm-config.toml &
WL_MITM=$!

for i in $(seq 1 5); do
    [ -S /xdg_runtime/wayland-10 ] && break
    sleep 1
done

if [ ! -S /xdg_runtime/wayland-10 ]; then
  echo "wl-mitm did not start"
  exit 1
fi

# XWayland Satellite (will listen on :1)
echo "Starting XWayland Satellite"
XDG_RUNTIME_DIR=/xdg_runtime WAYLAND_DISPLAY=wayland-10 xwayland-satellite :1 &
XWAYLAND_SATELLITE=$!

for i in $(seq 1 5); do
    [ -S /tmp/.X11-unix/X1 ] && break
    sleep 1
done

if [ ! -S /tmp/.X11-unix/X1 ]; then
  echo "XWayland Satellite did not start"
  exit 1
fi

wait $XWAYLAND_SATELLITE
wait $WL_MITM
