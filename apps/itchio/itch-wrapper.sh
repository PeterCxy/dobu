#!/usr/bin/env bash

itch-setup --prefer-launch -- --no-sandbox

sleep 10

# Wait until the real itch.io process exits
# Because whoever designed the client thought it is a good idea to unconditionally daemonize
for pid in $(pgrep itch); do
  ppid=$(ps -o ppid= -p $pid | xargs)
  if [ "$ppid" == "1" ]; then
    while [ -d /proc/$pid ]; do
      sleep 1
    done
  fi
done

