#!/usr/bin/env bash

script_path="$(dirname "$(realpath "$0")")"
. "$script_path/functions.sh"
assert_prerequisites

[ -z "$1" ] && die "Usage: create-shortcut.sh <app_name>"

image_name="$(relative_path_to_image_name "apps/$1")"
assert_image_exists "$image_name"

desktop_file_path="$(get_image_label "$image_name" "net.typeblog.dobu.desktop_file_path")"

[ -z "$desktop_file_path" ] && die "App $1 did not specify desktop_file_path in its labels"

# Create a temporary working directory
# Because we will have to extract files from the container image
tmp_dir="$(mktemp -d -p /tmp)"
pushd "$tmp_dir"

tmp_container_name="dobu-tmp-$(cat /dev/urandom | tr -dc '[:alpha:]' | fold -w 10 | head -n 1)"

# Let's set up cleanup tasks first
cleanup() {
  popd || true
  rm -rf "$tmp_dir" || true
  podman rm -f -v "$tmp_container_name" || true
}

trap cleanup EXIT

log "Creating temporary container $tmp_container_name from $image_name"

podman create --name "$tmp_container_name" "$image_name"

log "Extracting $desktop_file_path from $tmp_container_name"
podman cp "$tmp_container_name:$desktop_file_path" ./$1.desktop

log "Extracting /usr/share/icons from $tmp_container_name"
podman cp "$tmp_container_name:/usr/share/icons" ./icons

log "Destroying temporary container $tmp_container_name"
podman rm -f -v "$tmp_container_name"

# We delete everything after the first [Desktop Entry]
# because we don't intend to support all the weird actions an app might have
desktop_file_content="$(cat ./$1.desktop | awk '/^\[.*\]$/ && ++f == 2 { exit } 1')"

icon_name="$(echo "$desktop_file_content" | grep "Icon=" | head -1 | cut -d'=' -f 2)"
[ -z "$icon_name" ] && die "No icon defined for $1"

log "Searching for icon file for $1: $icon_name"

# Here we assume that $icon_name always refers to something in /usr/share/icons in the container
# When building images, make sure they are always placed there
icons=($(find ./icons -name "$icon_name.*"))
[ "${#icons[@]}" -eq 0 ] && die "No icons found for $icon_name"
for icon in "${icons[@]}"; do
  icon_target="${icon/.\/icons\/hicolor/$HOME/.local/share/icons/hicolor}"
  log "Copying $icon to $icon_target"
  mkdir -p "$(dirname "$icon_target")"
  cp "$icon" "$icon_target"
done

desktop_target="$HOME/.local/share/applications/$1.desktop"
log "Creating $desktop_target"
echo "$desktop_file_content" | grep -v "^TryExec=.*\$i" \
  | sed -r "s@^Exec=(.*)\$@Exec=$script_path/dobu-run.sh $1@" > "$desktop_target"

log "Desktop shortcut for $1 has been generated"
log "You may now need to refresh your desktop application list by re-logging in"
