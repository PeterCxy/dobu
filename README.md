Dobu
---

This is a collection of scripts and corresponding container images to run desktop Linux applications
(mainly games) in isolated environments using unprivileged (rootless) Podman.

Unlike Distrobox, Dobu uses exclusively pre-defined `Containerfile`s (`Dockerfile`s) and does not support
ad-hoc containers. All applications are defined using `Containerfile`s under the `apps/` directory.
Also unlike Distrobox, Dobu does not share everything accessible through the host graphics environment with
application containers. By default, Dobu only allows access to the following:

- Wayland / Xorg proxied through [sommelier](https://chromium.googlesource.com/chromiumos/platform2/+/refs/heads/main/vm_tools/sommelier/)
  - This gives containers effectively a separate Wayland subcompositor with its own Xwayland server,
    mitigating risks associated with sharing a desktop session
  - You can *optionally* grant access to raw Wayland / Xorg sockets for specific apps in `config.sh` in case
    of compatibility issues.
- DRI render nodes (for GL acceleration)
- PulseAudio / Pipewire server
  - This should ideally be proxied too like Wayland / Xorg, but currently no ideal implementation is known
- A user-defined, isolated, per-container `$HOME` directory
- Direct `/dev/input` access can be optionally granted

Requirements
---

- A __Wayland__ compositor: this is required for `sommelier`
- Unprivileged user namespaces support
  - This is to support Podman and some applications which require further namespacing inside the containers
  - Unprivileged user namespaces will be blocked by default inside application containers unless otherwise
    specified.
- Rootless Podman
- A PulseAudio-compatible audio server

Setup
---

Scripts here expect to be run straight from the repository. Before running any application, you will need to
build the `sommelier` image first, as it is run from its own isolated container too.

```shell
$ ./build-image.sh deps/sommelier
```

Then, take a look at `config-default.sh`, where some configurable options are located. This includes the base
path to `$HOME` directories for containers, and whether to allow direct `/dev/input` to some containers, etc.
If there is an option you would like to override, create a `config.sh` and include the override options there.

You should now be ready to run an application. Take a look at `apps/`, choose an application to run, and build
its image (using `prismlauncher` as an example):

```shell
$ ./build-image.sh apps/prismlauncher
$ ./dobu-run.sh apps/prismlauncher
```

Instead of manually executing `./dobu-run.sh` every time, you could also generate a desktop shortcut:

```shell
$ ./create-shortcut.sh apps/prismlauncher
```

Note that this shortcut is created on a best-effort basis. Not all features are supported, but you should at
least be able to launch an application through the shortcut.

Adding an Application
---

Adding an application should in general be straightforward if you are familiar with `Containerfile`s / `Dockerfile`s.
You can use existing applications under `apps/` as a template.

In general, all applications should use a base image available under `deps/`. Currently, this means either
`deps/base-archlinux`, tagged `dobu/deps-base-archlinux` when built, or `deps/base-ubuntu-jammy`, tagged
`dobu/deps-base-ubuntu-jammy` when built.

A `Containerfile` for a Dobu application looks in general like the following:

```Dockerfile
FROM dobu/deps-base-<archlinux|ubuntu-jammy>:latest

RUN <whatever you need to install the application>

# This is IMPORTANT because Dobu assumes "user" to be the default unprivileged username inside
# the container. This username will be mapped to the host user ID when a container is started.
USER user

# This is used by create-shortcut.sh to generate desktop entries outside of the container
LABEL net.typeblog.dobu.desktop_file_path="/path/to/desktop/file/in/container"
# If your application requires the use of unprivileged user namespaces even inside the
# container, set the following
# LABEL net.typeblog.dobu.unsafe_i_know_what_i_am_doing_allow_namespaces="true"

ENTRYPOINT [ "/path/to/main/app/binary" ]
```

Then, run `./build-image.sh apps/your-app` to create an image.

There is an additional `control` file that you could add in your app directory. Currently, there
are only a few supported options:

```bash
# If your application image is based on an Arch Linux package, setting this would tell Dobu
# to set an argument UPSTREAM_VERSION to the version of the indicated Arch Linux package when
# building the application image. If you refer to this version somewhere in your Containerfile
# (it could be as simple as `ARG UPSTREAM_VERSION` near the start of your Containerfile), this
# would invalidate Podman's build cache whenever this version changes. This allows these images
# to be updated with a simple rebuild instead of having to manually bump some version code
# every time something updates.
# Note that you SHALL NOT depend on the exact value of this UPSTREAM_VERSION variable. It should
# ONLY be used as a cache invalidation mechanism.
INVALIDATE_CACHE_UPSTREAM_ARCHLINUX="[core|extra|multilib]/package"
# Same thing, but for GitHub upstreams
INVALIDATE_CACHE_UPSTREAM_GITHUB="user/package"
# or Ubuntu
INVALIDATE_CACHE_UPSTREAM_UBUNTU="package"
# or AUR (Arch User Repository)
INVALIDATE_CACHE_UPSTREAM_AUR="some_package"
```

Limitations
---

- Dobu is currently created with x86\_64 and only x86\_64 in mind.
- Applications inside containers share the same UID as the host user. This is to make
  file sharing easier (otherwise you have to either `sudo` or `podman unshare` all the time),
  and to share things like the Sommelier socket and PulseAudio without making them
  world-writable or `setfacl` hacks.
- Some applications can show compatibility issues when running with Sommelier under some
  desktop environments. For example, popup dialogs might be positioned wrong. These cases
  can currently only be worked around by granting direct access to the raw display server
  in `config.sh` until Sommelier, the desktop environment, or the app improve.
- PulseAudio socket is not isolated but shared directly. This could ideally be solved by
  a proxy of PulseAudio similar to Sommelier for Wayland / Xorg.
- JACK is unsupported; however, Pipewire socket is shared by default just like PulseAudio,
  if it exists on the host. All apps requiring JACK is packaged with `pipewire-jack` in this
  repository.
- DBus is not shared at all into application containers. This means that input methods such
  as Fcitx would not work through their traditional DBus interfaces, and status indicators
  through `libappindicator` are broken (e.g. Steam).
  - I do not have a plan to support DBus inside containers. In my opinion, many of these
    limitations should be solved by Wayland protocols and be proxied through Sommelier.
    Once compositors, input methods, and Sommelier all support the `text_input` suite of
    protocols, they should work inside these containers.
