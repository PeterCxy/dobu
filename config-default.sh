# Where should the home directories used in containers be persisted
HOMEDIR_STORAGE=$HOME/.local/share/dobu/homedir
# Whether btrfs subvolumes should be used for homes instead of
# normal subdirectories
HOMEDIR_IS_BTRFS=false

# Array of app containers that are granted full /dev/input access
DEV_INPUT_APP_ALLOWLIST=()
# Array of devices that will be passed through to apps with input access
# If undefined, all input devices will be passed through
# Note that some devices expose multiple /dev/input interfaces,
# and all of them have to be in this allowlist to grant access
#DEV_INPUT_DEVICE_ALLOWLIST=(
#  js0
#  # or use device IDs -- in case device paths change across reboots or hotplug
#  by-id/usb-XXX-xxx
#)

# Array of app containers that are granted direct Wayland / X11 access
# apps not in this list will be graphically isolated using Sommelier
DISPLAY_SERVER_APP_ALLOWLIST=()

# Extra bind mounts per app container, one per line in the following format:
#   /path/on/host:/path/in/container
# Since the default username is `user` inside all packaged containers, you could
# assume `/home/user` is the home directory inside those containers.
# Note that any `-` in appname should be replaced with `_`
# Also note that the app name does not include the `app-` prefix.
#EXTRA_BIND_MOUNTS_appname=()
